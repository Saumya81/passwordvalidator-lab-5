package password_validator;

import static org.junit.Assert.*;

import org.junit.Test;


/*
 * 
 * ANUJ CHOUDHARY - 991539753
 * 
 * */
public class PasswordValidatorTest {

	@Test
	public final void testIsValidLengthRegular() {
		boolean result = PasswordValidator.isValidLength("12ABCDEF45");
		assertTrue("Valid Passsword",result==true); // TODO
	}
	
	
	
	@Test 
	public void testIsValidLengthException() {
		boolean result=PasswordValidator.isValidLength("");
		assertFalse("Invalid password length",result);
	}
	
	
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		boolean result=PasswordValidator.isValidLength("12ABCDEFG");	
		assertTrue("The time provided does not match the result",result);
	}
	
	
	
	
	@Test
	public void testIsValidLengthBoundaryOut() {
		boolean result=PasswordValidator.isValidLength("12MAMIOP ");	
		assertFalse("The time provided does not match the result",result);
	}


}
