package password_validator;

public class PasswordValidator {

	private static int MIN_LENGTH = 8;
	
	
	public static boolean isValidLength(String password)
	{
		boolean hasDigits=false;
		int totalDigits=0;
		
		for(int index = 0; index < password.length(); index++ ){

	        if( Character.isDigit(password.charAt( index )) ){
	            totalDigits++;
	        } 

	    } 
		
		if(totalDigits>=2)
		{
			hasDigits=true;
		}
		else
		{
			hasDigits=false;
		}
		
		// A password must contain at least two digits
		return password.indexOf(" ") < 0 && password.length() >= MIN_LENGTH && hasDigits;
	}
}
